package com.leandro.crawler.feed.security;

import javax.annotation.Priority;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

@Secured
@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilter implements ContainerRequestFilter {

    private static final String AUTHENTICATION_SCHEME = "Bearer";
    private static final Response ACCESS_DENIED = Response.status(Response.Status.UNAUTHORIZED).build(); //401

    @Override
    public void filter(final ContainerRequestContext requestContext) throws IOException {
        // Get the HTTP Authorization header from the request
        final String authorizationHeader =
                requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);

        // Check if the HTTP Authorization header is present and formatted correctly
        if (authorizationHeader == null || !authorizationHeader.startsWith(AUTHENTICATION_SCHEME)) {
            requestContext.abortWith(ACCESS_DENIED);
            throw new NotAuthorizedException("Authorization header must be provided");
        }

        // Extract the username and password from the HTTP Authorization header
        final String token = authorizationHeader.substring(AUTHENTICATION_SCHEME.length()).trim();

        if(!Token.validateToken(token)){
            requestContext.abortWith(ACCESS_DENIED);
            throw new NotAuthorizedException("Authorization provided is invalid");
        }
    }
}
