package com.leandro.crawler.feed.security;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.MacProvider;

import java.security.Key;

public class Token {

    private static String tokenString;
    private static final  Key key = MacProvider.generateKey();

    public static String getToken() {
        if (tokenString == null) {

            if (key != null) {
                tokenString = Jwts.builder()
                        .setSubject("Globo")
                        .signWith(SignatureAlgorithm.HS512, key)
                        .compact();
            }
        }
        return tokenString;
    }

    public static boolean validateToken(String tokenString){
        return Jwts.parser().setSigningKey(key).
                parseClaimsJws(tokenString).getBody().getSubject().equals("Globo");
    }

}
