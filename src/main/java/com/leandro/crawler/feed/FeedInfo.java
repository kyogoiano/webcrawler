package com.leandro.crawler.feed;

import java.io.Serializable;
import java.net.URL;
import java.util.Collection;
import java.util.Date;

public class FeedInfo implements Serializable{

	private String title;
	private Date lastFetchDate;
	private URL feedUrl;
	private URL htmlUrl;
	private Collection<Item> items;


	public Date getLastFetchDate() {
		return lastFetchDate;
	}

	public void setLastFetchDate(final Date lastFetchDate) {
		this.lastFetchDate = lastFetchDate;
	}

	public URL getFeedUrl() {
		return feedUrl;
	}

	public void setFeedUrl(final URL feedUrl) {
		this.feedUrl = feedUrl;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}


	public URL getHtmlUrl() {
		return htmlUrl;
	}

	public void setHtmlUrl(final URL htmlUrl) {
		this.htmlUrl = htmlUrl;
	}

	public Collection<Item> getItems() {
		return items;
	}

	public void setItems(Collection<Item> items) {
		this.items = items;
	}

	@Override
	  public String toString() {
	    return "FeedInfo [title=" + title + ", lastFetchDate="
	        + lastFetchDate + ", feedUrl=" + feedUrl + ", htmlUrl=" + htmlUrl + "]";
	  }

}
