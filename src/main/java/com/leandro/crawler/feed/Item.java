package com.leandro.crawler.feed;

import java.io.Serializable;
import java.util.List;

public class Item implements Serializable {

    private String title;

    private String link;

    private List<Description> descriptions;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

	public List<Description> getDescription() {
		return descriptions;
	}

	public void setDescriptions(List<Description> descriptions) {
		this.descriptions = descriptions;
	}
}
