package com.leandro.crawler.feed.startup;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Destroyed;
import javax.enterprise.context.Initialized;
import javax.enterprise.event.Observes;

import com.leandro.crawler.reader.FeedReader;

@ApplicationScoped
public class FeedStartupLoader {

	public void init(@Observes @Initialized(ApplicationScoped.class) Object init) {
        //Start Feed Reader
	    FeedReader.main();
    }
 
    public void destroy(@Observes @Destroyed(ApplicationScoped.class) Object init) {
       FeedReader.closeApp();
    }
	
}
