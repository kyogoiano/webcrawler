package com.leandro.crawler.feed.agent.impl;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.zip.GZIPInputStream;

import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.concurrent.FutureCallback;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.apache.http.nio.reactor.IOReactorException;

import com.leandro.crawler.feed.FeedInfo;
import com.leandro.crawler.feed.agent.CrawlerAgent;
import com.leandro.crawler.feed.callback.Callback;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.FeedException;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;

public class ClientCrawlerAgent implements CrawlerAgent{


    private final CloseableHttpAsyncClient httpAsyncClient;
    private RequestConfig requestConfig;

    public ClientCrawlerAgent(final HttpHost proxyHost) throws IOReactorException {
        this(HttpAsyncClients.createDefault(), proxyHost);
        
    }
    
    public ClientCrawlerAgent(final CloseableHttpAsyncClient httpAsyncClient, final HttpHost proxyHost) {
        this.httpAsyncClient = httpAsyncClient;
        this.httpAsyncClient.start();
        if(proxyHost != null) {
        	this.requestConfig = RequestConfig.custom().setProxy(proxyHost).build();
        } else {
            this.requestConfig = RequestConfig.custom().build();
        }
    }

    @Override
    public void request(final FeedInfo feedInfo, final Callback callback) throws  ExecutionException, InterruptedException {
        final Future<HttpResponse> future =
                httpAsyncClient.execute(buildGetRequest(feedInfo), new CallbackWrapper(callback));

        HttpResponse response = future.get();

        if(response.getStatusLine().getStatusCode() == 200){
            System.out.println("Request OK!");
        }

    }

    private HttpUriRequest buildGetRequest(final FeedInfo feedInfo) {
        try {
        	final HttpGet httpGet = new HttpGet(feedInfo.getFeedUrl().toURI());
        	httpGet.setConfig(requestConfig);
			return httpGet;
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    static class CallbackWrapper implements FutureCallback<HttpResponse> {
        private final Callback callback;

        public CallbackWrapper(final Callback cb) {
            callback = cb;
        }

        @Override
        public void completed(final HttpResponse result) {
            try {
                callback.completed(buildFeed(result));
            } catch (Exception e) {
                failed(e);
            }
        }

        private SyndFeed buildFeed(final HttpResponse result) throws IOException,
            FeedException {
            InputStream stream = null;
            try {
                stream = result.getEntity().getContent();
                final Header contentEnc = result.getFirstHeader("Content-Encoding");
                if (contentEnc != null
                        && "gzip".equalsIgnoreCase(contentEnc.getValue())) {
                    stream = new GZIPInputStream(stream);
                }

                XmlReader reader;
                final Header contentType = result.getEntity().getContentType();
                if (contentType == null) {
                    reader = new XmlReader(result.getEntity().getContent(),
                            true);
                } else {
                    reader = new XmlReader(result.getEntity().getContent(),
                            contentType.getValue(), true);
                }

                return new SyndFeedInput().build(reader);
            } finally {
                if (stream != null) {
                    stream.close();
                } 
            }
        }

        @Override
        public void failed(final Exception ex) {
            callback.failed(ex);
        }

        @Override
        public void cancelled() {
            try {
                callback.cancelled();
            } catch (Exception e) {
                failed(e);
            }
        }
    }

	@Override
	public void close() throws IOException {
		this.httpAsyncClient.close();
		
	}

	
}
