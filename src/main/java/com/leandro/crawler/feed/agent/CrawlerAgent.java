package com.leandro.crawler.feed.agent;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import com.leandro.crawler.feed.FeedInfo;
import com.leandro.crawler.feed.callback.Callback;

public interface CrawlerAgent {

    void request(FeedInfo feedInfo, Callback callback) throws ExecutionException, InterruptedException;
    
    void close() throws IOException;
    
}
