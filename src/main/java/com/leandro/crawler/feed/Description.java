package com.leandro.crawler.feed;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

public class Description  implements Serializable {

    private String type;

    private Collection<String> content;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Collection<String> getContent() {
        return content;
    }

    public void setContent(List<String> content) {
        this.content = content;
    }
}
