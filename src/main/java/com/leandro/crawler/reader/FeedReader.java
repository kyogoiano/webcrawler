package com.leandro.crawler.reader;

import com.leandro.crawler.feed.Description;
import com.leandro.crawler.feed.FeedInfo;
import com.leandro.crawler.feed.Item;
import com.leandro.crawler.feed.agent.CrawlerAgent;
import com.leandro.crawler.feed.agent.impl.ClientCrawlerAgent;
import com.leandro.crawler.feed.callback.Callback;
import com.rometools.rome.feed.synd.SyndContent;
import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;

import org.apache.http.nio.reactor.IOReactorException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class FeedReader {

	private static final Logger LOG = LoggerFactory.getLogger(FeedReader.class);

	private static CrawlerAgent crawlerAgent;

	private static final FeedInfo feedInfo = new FeedInfo();

	public static void main(final String... args) {
		
        //final HttpHost proxyHost = new HttpHost("ncproxy1", 8080, "http");
		try {
			//crawlerAgent = new ClientCrawlerAgent(proxyHost);
			crawlerAgent = new ClientCrawlerAgent(null);
		} catch (final IOReactorException ex) {
			ex.printStackTrace();
			LOG.error("ERROR: "+ex.getMessage());
		}
		
		try {
			feedInfo.setFeedUrl(new URL("http://revistaautoesporte.globo.com/rss/ultimas/feed.xml"));
		} catch (MalformedURLException ex) {
			ex.printStackTrace();
			LOG.error("ERROR: "+ex.getMessage());
		}
		
		final Callback callback =  new FeedInfoUpdateCallback(feedInfo);

		try {
			crawlerAgent.request(feedInfo, callback);
		} catch (InterruptedException | ExecutionException ex) {
            ex.printStackTrace();
			LOG.error("ERROR: "+ex.getMessage());
        }
	}
	
	public static void closeApp() {
		if (crawlerAgent != null) {
			try {
				crawlerAgent.close();
			} catch (IOException ex) {
				LOG.error("ERROR: "+ex.getMessage());
				ex.printStackTrace();
			}
		}
	}
	
	public static FeedInfo getFeedinfo() {
		return feedInfo;
	}

	static class FeedInfoUpdateCallback implements Callback {
		final private FeedInfo feedInfo;

		public FeedInfoUpdateCallback(final FeedInfo feedInfo) {
			this.feedInfo = feedInfo;
		}

		@Override
		public void completed(final SyndFeed feed) {
			LOG.info("request completed: {}", feedInfo);

			try {
				updateFeedInfo(feed);
			} catch (MalformedURLException e) {
				failed(e);
			}
		}

		private void updateFeedInfo(final SyndFeed feed) throws MalformedURLException {
			feedInfo.setLastFetchDate(new Date());
			feedInfo.setTitle(feed.getTitle());
			feedInfo.setHtmlUrl(new URL(feed.getLink()));

			if(feed.getEntries() != null) {
                final Collection<Item> items = new ArrayList<>();
                for (final SyndEntry entry : feed.getEntries()) {
                    final Item item = new Item();
                    item.setTitle(entry.getTitle());
                    item.setLink(entry.getLink());

                    final SyndContent syndContent = entry.getDescription();
                    if(syndContent != null &&
                            syndContent.getType().equals("text/html")){
                        //parse html
                        final Document document = Jsoup.parse(syndContent.getValue());
                        final Elements allElements = document.select("p, img[src], a"); //TODO: links should be tag ul ???
                        // separate all elements by type
                        final List<Description> descriptions = new ArrayList<>();
                        for(final Element element : allElements) {
                        	final Description description = new Description();
                        	final Elements textElements = element.getElementsByTag("p");
                        	if(!textElements.isEmpty()) {
                        		description.setType("text");
                        		final List<String> textList = new ArrayList<>();
                        		for(final Element textElement : textElements) {
                        			if(!textElement.text().isEmpty()) {
                        				textList.add(textElement.text());
                        			}
                        		}
                        		description.setContent(textList);
                        	}
                        	final Elements imageElements = element.select("img[src]");
                        	if(!imageElements.isEmpty()) {
                        		description.setType("image");
                        		final List<String> imageList = new ArrayList<>();
                        		for(final Element imageElement : imageElements) {
                        			imageList.add(imageElement.attributes().get("src"));
                        		}
                        		description.setContent(imageList);
                        	}
                        	final Elements linkElements = element.getElementsByTag("a");
                        	if(!linkElements.isEmpty()) {
                        		description.setType("links");
                        		final List<String> linkList = new ArrayList<>();
                        		for(final Element linkElement : linkElements) {
                        			linkList.add(linkElement.attributes().get("href"));
                        		}
                        		description.setContent(linkList);
                        	}
                        	if(!description.getContent().isEmpty()) {
                        		descriptions.add(description);
                        	}
                        }
                        item.setDescriptions(descriptions);
                    }
                    items.add(item);
                }
                feedInfo.setItems(items);
            }
		}

		@Override
		public void failed(final Exception ex) {
			LOG.warn("request faildure: {}", feedInfo);
		}

		@Override
		public void cancelled() {
			LOG.warn("request canlled: {}", feedInfo);
		}

		@Override
		public FeedInfo getFeedInfo() {
			return feedInfo;
		}
	}
}
