package com.leandro.crawler.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.leandro.crawler.feed.FeedInfo;
import com.leandro.crawler.reader.FeedReader;

@Path("/")
public class FeedEndpoint {
	
	@GET
    @Produces(MediaType.APPLICATION_JSON)
	public FeedInfo getFeed() {
		return FeedReader.getFeedinfo();
	}


}
