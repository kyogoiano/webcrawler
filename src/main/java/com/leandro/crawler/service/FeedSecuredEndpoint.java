package com.leandro.crawler.service;

import com.leandro.crawler.feed.FeedInfo;
import com.leandro.crawler.feed.security.Secured;
import com.leandro.crawler.reader.FeedReader;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Path("/secured")
public class FeedSecuredEndpoint {

    @GET
    @Secured
    @Produces(MediaType.APPLICATION_JSON)
    public FeedInfo getFeedAuthenticated() {
        return FeedReader.getFeedinfo();
    }
}
