package com.leandro.crawler.service;

import com.leandro.crawler.feed.security.Credentials;
import com.leandro.crawler.feed.security.Token;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/authenticate")
public class AuthenticationEndpoint {

    @POST
    @Produces("text/plain")
    @Consumes("application/json")
    public String authenticateUser(Credentials credentials) {

        final String username = credentials.getUserName();
        final String password = credentials.getPassword();

        if(username.equals("globo") && password.equals("infoglobo")){
            // Issue a token and return a response
            return Token.getToken();
        } else {
            return  "ERROR: Invalid user or password!!!";
        }
    }
}
